package com.deepak.eamclient.service;

import org.springframework.stereotype.Service;

import com.deepak.eamclient.model.Employee;

@Service
public interface EmployeeService {
    public Employee getEmployeeDetails();
}
