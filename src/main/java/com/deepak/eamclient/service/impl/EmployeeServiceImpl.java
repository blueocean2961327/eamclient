package com.deepak.eamclient.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.deepak.eamclient.service.EmployeeService;
import com.deepak.eamclient.model.Address;
import com.deepak.eamclient.model.Employee;
import com.deepak.eamclient.model.Name;
import com.deepak.eamclient.model.Project;

@Service
public class EmployeeServiceImpl implements EmployeeService{
    
   
    @Override
    public Employee getEmployeeDetails() {
        Name empName = new Name("Deepak", "Kumar", "Sadangi", "Chinku", "Employee Name");
        List<Name> parentsName = new ArrayList<>();
        Name empFatherName = new Name("Deba", "Prasad", "Sadangi", "Jhuna", "Employee Father Name");
        Name empMotherName = new Name("Kumudini", "", "Sadangi", "KUMI", "Employee Mother Name");
        Name empBrotherName = new Name("Rabi", "Narayan", "Sadangi", "Bapi", "Employee Brother Name");
        Name empSisterInLawName = new Name("Sibani", "", "Ratha", "Chitra", "Employee Sister-In-Law Name");
        
        parentsName.addAll(List.of(empFatherName, empMotherName, empBrotherName, empSisterInLawName));
        
        List<Address> adressList = new ArrayList<>();
        Address permanentAddress = new Address("Permanent Address","C/O Debaprasad Sadangi","Amalapada, GoudaSahi", "Near Dr.Raj Kishore Mishra Homeo Clinic ", "PHULBANI", "762001", "Odisha", "INDIA" );
        Address currentAddress = new Address("Current Address","Sri Bhabhani PG","", "Near Cisco Office ", "Bangalore", "560103", "Karnataka", "INDIA" );
        adressList.addAll(List.of(permanentAddress,currentAddress));
        
        List<Project> projects = new ArrayList<>();
        Project erricssion = new Project("Gateway", "Telecom Domain", "E1234", "01-01-2022", "28-01-2023", "128");
        Project cisco = new Project("wireless", "Telecom Domain", "C1236", "05-04-2023", "15-08-2023", "7");
        projects.addAll(List.of(erricssion, cisco));
        
        Employee employee = new Employee(empName, parentsName, adressList, "123456789", "15-04-2023", projects);
        System.out.println(employee);
        return employee;
    }

}
