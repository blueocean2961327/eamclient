package com.deepak.eamclient.util;

import org.springframework.http.HttpHeaders;

public class WebUtils {
    
    public static HttpHeaders createHeaders() {

        HttpHeaders header = new HttpHeaders();
        header.set("Content-Type", "multipart/form-data");
        header.add("Accept", "application/json");

        return header;
    }
}
