package com.deepak.eamclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class EamclientApplication {

	public static void main(String[] args) {
		SpringApplication.run(EamclientApplication.class, args);
	}

	@Bean
	public RestTemplate getRestTempolate() {
		return new RestTemplate();

	}

}
