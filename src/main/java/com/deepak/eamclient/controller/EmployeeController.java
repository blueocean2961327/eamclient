package com.deepak.eamclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.deepak.eamclient.service.impl.EmployeeServiceImpl;
import com.deepak.eamclient.model.Employee;
import com.deepak.eamclient.util.WebUtils;

@RestController
@RequestMapping("/eam")
public class EmployeeController {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    EmployeeServiceImpl employeeServiceImpl;

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable String id) {
        ResponseEntity<Employee> res = new ResponseEntity<Employee>(employeeServiceImpl.getEmployeeDetails(),
                HttpStatus.OK);
        return res;

    }

}
