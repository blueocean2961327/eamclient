package com.deepak.eamclient.model;

public class Project {
    private String projectName;
    private String projectDescription;
    private String projectCode;
    private String projectStartDate;
    private String projectEndDate;
    private String projectResourceSize;
    
    public Project() {
        super();
    }

    public Project(String projectName, String projectDescription, String projectCode, String projectStartDate,
            String projectEndDate, String projectResourceSize) {
        super();
        this.projectName = projectName;
        this.projectDescription = projectDescription;
        this.projectCode = projectCode;
        this.projectStartDate = projectStartDate;
        this.projectEndDate = projectEndDate;
        this.projectResourceSize = projectResourceSize;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getProjectStartDate() {
        return projectStartDate;
    }

    public void setProjectStartDate(String projectStartDate) {
        this.projectStartDate = projectStartDate;
    }

    public String getProjectEndDate() {
        return projectEndDate;
    }

    public void setProjectEndDate(String projectEndDate) {
        this.projectEndDate = projectEndDate;
    }

    public String getProjectResourceSize() {
        return projectResourceSize;
    }

    public void setProjectResourceSize(String projectResourceSize) {
        this.projectResourceSize = projectResourceSize;
    }

    @Override
    public String toString() {
        return "Project [projectName=" + projectName + ", projectDescription=" + projectDescription + ", projectCode="
                + projectCode + ", projectStartDate=" + projectStartDate + ", projectEndDate=" + projectEndDate
                + ", projectResourceSize=" + projectResourceSize + "]";
    }
    
    

}
