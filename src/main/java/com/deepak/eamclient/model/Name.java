package com.deepak.eamclient.model;

public class Name {
    String firstName;
    String middleName;
    String lastName;
    String nickName;
    String type;

    public Name() {
        super();
    }

    

    public Name(String firstName, String middleName, String lastName, String nickName, String type) {
        super();
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.nickName = nickName;
        this.type = type;
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Name [firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", nickName="
                + nickName + ", type=" + type + "]";
    }

    
}
