package com.deepak.eamclient.model;

import java.util.List;

public class Employee {
    private Name name; 
    private List<Name> parentsName;
    private List<Address> address;  
    private String empCode;
    private String doj;
    private List<Project> projects;
    
    
    public Employee() {
        super();
    }

    public Employee(Name name, List<Name> parentsName, List<Address> address, String empCode, String doj,
            List<Project> projects) {
        super();
        this.name = name;
        this.parentsName = parentsName;
        this.address = address;
        this.empCode = empCode;
        this.doj = doj;
        this.projects = projects;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public List<Name> getParentsName() {
        return parentsName;
    }

    public void setParentsName(List<Name> parentsName) {
        this.parentsName = parentsName;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getDoj() {
        return doj;
    }

    public void setDoj(String doj) {
        this.doj = doj;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "Employee [name=" + name + ", parentsName=" + parentsName + ", address=" + address + ", empCode="
                + empCode + ", doj=" + doj + ", projects=" + projects + "]";
    }

   
}
