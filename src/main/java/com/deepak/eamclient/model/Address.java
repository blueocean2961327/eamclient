package com.deepak.eamclient.model;

public class Address {
    String line1;
    String line2;
    String landMark;
    String addressType;
    String pin;
    String state;
    String country;
    String city;
    
    
    public Address() {
        super();
    }


    public Address( String addressType,String line1, String line2, String landMark, String city, String pin, String state,
            String country) {
        super();
        this.line1 = line1;
        this.line2 = line2;
        this.landMark = landMark;
        this.addressType = addressType;
        this.pin = pin;
        this.state = state;
        this.country = country;
        this.city = city;
    }


    public String getLine1() {
        return line1;
    }


    public void setLine1(String line1) {
        this.line1 = line1;
    }


    public String getLine2() {
        return line2;
    }


    public void setLine2(String line2) {
        this.line2 = line2;
    }


    public String getLandMark() {
        return landMark;
    }


    public void setLandMark(String landMark) {
        this.landMark = landMark;
    }


    public String getAddressType() {
        return addressType;
    }


    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }


    public String getPin() {
        return pin;
    }


    public void setPin(String pin) {
        this.pin = pin;
    }


    public String getState() {
        return state;
    }


    public void setState(String state) {
        this.state = state;
    }


    public String getCountry() {
        return country;
    }


    public void setCountry(String country) {
        this.country = country;
    }


    public String getCity() {
        return city;
    }


    public void setCity(String city) {
        this.city = city;
    }


    @Override
    public String toString() {
        return "Address [line1=" + line1 + ", line2=" + line2 + ", landMark=" + landMark + ", addressType="
                + addressType + ", pin=" + pin + ", state=" + state + ", country=" + country + ", city=" + city + "]";
    }

}
